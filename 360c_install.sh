#!/usr/bin/env bash
set -e

if [ "$#" -ne 1 ];
then
	echo "Definire il percorso d'installazione del framework."
	exit
fi

git clone git@bitbucket.org:360consulenza/360c-symfony-framework.git $1

cd $1

composer install --no-dev

php app/console doctrine:schema:create
php app/console doctrine:fixtures:load

echo $'\n\nInstallazione del framework completata!'
